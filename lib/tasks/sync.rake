desc "Sync Gitlab to DB"
task :gitlab_sync => :environment do
	Issue.all.each do |issue|
		Delayed::Job.enqueue(SyncJob.new(issue))
	end
end