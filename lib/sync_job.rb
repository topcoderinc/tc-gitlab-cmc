class SyncJob < Struct.new(:issue)
	# We are pulling the data down from gitlab into the db
	def perform
      gitlab_issue = $Gitlab_client.issue(issue.project.gitlab_id, issue.gitlab_id)

      status  = gitlab_issue.state
      labels = gitlab_issue.labels || " "
      if gitlab_issue.milestone
      	milestone = gitlab_issue.milestone.title	
      else
      	milestone = " "
	  end

      issue.title = gitlab_issue.title
	  issue.status = status
 	  issue.tags = labels.join(',')
      issue.milestone = milestone

      issue.save
	end
end